Name:        hunspell-uk
Version:     1.8.0
Release:     4
Summary:     Hunspell dictionaries for Ukrainian
License:     GPLv2+ or LGPLv2+ or MPLv1.1
URL:         http://sourceforge.net/projects/ispell-uk
Source:      https://downloads.sourceforge.net/project/ispell-uk/spell-uk/%{version}/dict-uk_UA-%{version}.oxt
BuildArch:   noarch
Requires:    hunspell
Supplements: (hunspell and langpacks-uk)
%description
Hunspell dictionaries for Ukrainian.

%prep
%autosetup -c -n hunspell-uk-%{version} -p1

%build

%install
install -Dp uk_UA/uk_UA.aff $RPM_BUILD_ROOT/%{_datadir}/myspell/uk_UA.aff
install -Dp uk_UA/uk_UA.dic $RPM_BUILD_ROOT/%{_datadir}/myspell/uk_UA.dic

%files
%doc uk_UA/README_uk_UA.txt
%{_datadir}/myspell/*

%changelog
* Tue Apr 30 2024 caodongxia <caodongxia@h-partners.com> - 1.8.0-4
- add hunspell-uk.yaml

* Tue Apr 28 2020 huanghaitao <huanghaitao8@huawei.com> - 1.8.0-3
- package init
